import { FC } from 'react'

import Sidebar from '@/components/layout/Sidebar/Sidebar'
import Navigation from '@/components/layout/Navigation/Navigation'

import styles from './Layout.module.scss'
import { API_URL } from '../../config/api.config'

const Layout: FC = ({ children }) => {
	return (
		<div className={styles.layout}>
			{/*{API_URL}*/}
			<Navigation />
			<div className={styles.center}>
				{children}
			</div>
			<Sidebar />
		</div>
	)
}

export default Layout