import { FC } from 'react'
import Link from 'next/link'
import Image from 'next/image'

import logoImage from '@/assets/images/logo.png'

const Logo: FC = () => {
	return (
		<Link href='/'>
			<a className='px-layout mb-10 block'>
				<Image src={logoImage} alt='Online cinema'/>
			</a>
		</Link>
	)
}

export default Logo