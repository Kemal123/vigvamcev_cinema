import { FC } from 'react'
import styles from './Search.module.scss'
import useSearch from '@/components/layout/Sidebar/Search/useSearch'
import SearchList from '@/components/layout/Sidebar/Search/SearchList/SearchList'
import SearchField from '@/ui/search_field/SearchField'

const Search: FC = () => {
	const { isSuccess, handleSearch, data, searchTerm } = useSearch()

	return (
		<div className={styles.wrapper}>
			<SearchField searchTerm={searchTerm} handleSearch={handleSearch} />
			{isSuccess && <SearchList movies={data || []} />}
		</div>
	)
}

export default Search