import { ChangeEvent, useState } from 'react'
import { useDebounce } from '@/hooks/useDebounce'
import { useQuery } from 'react-query'
import { MovieService } from '@/services/movie.service'

//поиск через reactQuery
export const useSearch = () => {
	const [searchTerm, setSearchTerm] = useState('')
	const debouncedSearch = useDebounce(searchTerm, 500)

	const { isSuccess, data } = useQuery(['search movie list', debouncedSearch], () =>
		MovieService.getAll(debouncedSearch), {
		select: ({ data }) => data,
		enabled: !!debouncedSearch,//только тогда, когда человек что-то вводит
	})

	const handleSearch = (e: ChangeEvent<HTMLInputElement>) => {
		setSearchTerm(e.target.value)
	}

	return {isSuccess, handleSearch, data, searchTerm}
}


export default useSearch