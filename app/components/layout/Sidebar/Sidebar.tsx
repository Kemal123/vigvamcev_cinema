import styles from './Sidebar.module.scss'
import { FC } from 'react'
import Search from '@/components/layout/Sidebar/Search/Search'

const Sidebar: FC = () => {
	return (
		<div className={styles.sidebar}>
			<Search />
			{/*movies container*/}
		</div>
	)
}

export default Sidebar