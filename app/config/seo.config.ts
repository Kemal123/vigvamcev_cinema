export const siteName = 'vigvamcev cinema'
export const titleMerge = (title: string) => `${title} | ${siteName}`
