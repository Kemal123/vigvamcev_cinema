import { TypeMaterialIconName } from './icon.type'

export interface IGenre {
	_id: string
	name: string
	slug: string
	description: string
	icon: TypeMaterialIconName
}

export interface IActor {
	_id: string
	photo: string
	name: string
	countMovies: number
	slug: string
}

export interface IParameters {
	year: number
	duration: number
	country: string
}

export interface IGenre {
	_id: string
	name: string
	slug: string
	description: string
	icon: TypeMaterialIconName
}

export interface IMovie {
	_id: string
	poster: string//вертикальный для листа
	bigPoster: string//горизонтальный
	title: string
	parameters: IParameters
	genres: IGenre[]
	actors: IActor[]
	countOpened: number//для количества просмотров странички фильма
	videoUrl: string
	rating: number
	slug: string
}